-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 05, 2021 at 02:11 PM
-- Server version: 8.0.21-0ubuntu0.20.04.4
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pcstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` int NOT NULL,
  `title` varchar(100) NOT NULL,
  `author` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint NOT NULL,
  `price` int NOT NULL,
  `quantity` int NOT NULL,
  `active_photo` varchar(255) NOT NULL,
  `date` datetime DEFAULT NULL,
  `add_by` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `books_photo`
--

CREATE TABLE `books_photo` (
  `id` int NOT NULL,
  `photo_id` int NOT NULL,
  `original_photo` varchar(255) NOT NULL,
  `cropped_photo` varchar(255) NOT NULL,
  `is_active` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category_books`
--

CREATE TABLE `category_books` (
  `id` int NOT NULL,
  `date` date DEFAULT NULL,
  `prio` int DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  `keyword` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category_book_links`
--

CREATE TABLE `category_book_links` (
  `id` int NOT NULL,
  `external_id` int DEFAULT NULL,
  `object_id` int DEFAULT NULL,
  `object_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `object_keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `st_order_customer`
--

CREATE TABLE `st_order_customer` (
  `id` int NOT NULL,
  `Name` varchar(255) NOT NULL,
  `FamilyName` varchar(255) NOT NULL,
  `Mail` varchar(255) NOT NULL,
  `PhoneNumber` int NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Comment` varchar(255) NOT NULL,
  `office` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `Time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `totalPrice` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `st_order_product`
--

CREATE TABLE `st_order_product` (
  `id` int NOT NULL,
  `createdBy` int NOT NULL,
  `productId` int NOT NULL,
  `quantity` int NOT NULL,
  `totalPrice` int NOT NULL,
  `dateAdded` int NOT NULL,
  `order_status` varchar(250) NOT NULL DEFAULT 'pending_order',
  `items` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `st_user_acc`
--

CREATE TABLE `st_user_acc` (
  `id` int NOT NULL,
  `username` varchar(255) NOT NULL,
  `usrFamName` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phoneNumber` int NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_created` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `is_active` tinyint(1) NOT NULL,
  `user_status` varchar(15) NOT NULL DEFAULT 'user',
  `user_level` int NOT NULL DEFAULT '110',
  `activated` tinyint NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `st_user_adress`
--

CREATE TABLE `st_user_adress` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `AddrArea` varchar(255) NOT NULL,
  `AddrCity` varchar(255) NOT NULL,
  `AddrNeibr` varchar(255) NOT NULL,
  `Addr` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `st_user_photos`
--

CREATE TABLE `st_user_photos` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `original_photo` varchar(255) NOT NULL,
  `cropped_photo` varchar(255) NOT NULL,
  `is_active` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books_photo`
--
ALTER TABLE `books_photo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_books`
--
ALTER TABLE `category_books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_book_links`
--
ALTER TABLE `category_book_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `st_order_customer`
--
ALTER TABLE `st_order_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `st_order_product`
--
ALTER TABLE `st_order_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `st_user_acc`
--
ALTER TABLE `st_user_acc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `st_user_adress`
--
ALTER TABLE `st_user_adress`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `st_user_photos`
--
ALTER TABLE `st_user_photos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `books_photo`
--
ALTER TABLE `books_photo`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category_books`
--
ALTER TABLE `category_books`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category_book_links`
--
ALTER TABLE `category_book_links`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `st_order_customer`
--
ALTER TABLE `st_order_customer`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `st_order_product`
--
ALTER TABLE `st_order_product`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `st_user_acc`
--
ALTER TABLE `st_user_acc`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `st_user_adress`
--
ALTER TABLE `st_user_adress`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `st_user_photos`
--
ALTER TABLE `st_user_photos`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
