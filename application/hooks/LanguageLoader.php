<?php if ( ! defined('BASEPATH')) exit('Direct access allowed');

class LanguageLoader{
    public function initialize(){
        $ci =& get_instance();
        $ci ->load->helper('language');
        $siteLang = $ci->session->userdata('site_lang');
        if ($siteLang == "english") {
            $ci->lang->load('en','english');
        }else{
            $ci->lang->load('bg','bulgarian');
        }
    }
}
