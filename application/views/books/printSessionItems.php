
<?php $this->load->view('templates/header');  ?>

<div class="container">


	<table class="table table-bordered">
		<thead>
			<tr>
				<th>   
					<?php echo lang("title");?>
				</th>
				<th>
					<?php echo lang("quantity");?>
				</th>
				<th>
					<?php echo lang("price");?>
				</th>
				<th>
					<?php echo lang("final_price");?>
				</th>
				<th>
					<?php echo lang("remove_element");?>
				</th>
			</tr>
		
		</thead>
		<tbody>

		<?php
			// echo '<pre>',print_r($itemTotalPrice,1),'</pre>'; 

			// echo '<pre>'.print_r($bookInfo,1).'</pre>';
			// echo '<pre>',print_r($bookInfo,1),'</pre>';
			if(isset($bookInfo)){
				foreach($bookInfo as $key => $item){


				echo '
					
						<tr>
							<td>
								<div>
									'.$item[0]['title'].'
								</div>
							</td>
							<td>
								<div id="author">
									<input class="addQuantityToCart" type="number" min="1" max='.$item[0]['quantity'].' data-id="'.$item[0]['id'].'" value="'.$quantity[$key].'">
								</div>
							</td>
							<td>
								<div>
									'.$item[0]['price'].'
								</div>
							</td>
							<td>
								<div id="'.$item[0]['id'].'">
									'.$itemTotalPrice[$key].'
								</div>
							</td>
							<td>
								<button class="btn btn-danger remove_from_cart" data-id="'.$item[0]['id'].'"> 
									Remove
								</button>
							</td>
						</tr>
					';

			}
			} else {
				echo '.lang("cartEmpty").';
			}
		?>
    
	</tbody>
	</table>

<button class="btn btn-success" style="display: block; margin: 75 auto; text-align: center;" onclick="location.href='/buyitemsincart/registerOrderName';"> 
	<?php echo lang("proceed_with_order");?>
	<span class="totalPrice"> 
		<?=$totalPrice?> 
	</span> <?php echo lang("currency");?>. 
</button>




 
<script>
$( '.remove_from_cart' ).on('click', function(e) {
    e.preventDefault();
    // location.reload();
    var book_id = $(this).attr('data-id');

        $.ajax({
            type: "POST",   
            url: `/books/removeItemsFromSession/` + book_id,
            dataType: 'json',
            // data: data, 
        })
            .done(function(data) {
                    if(data.data === "deleted"){
                        alert('<?php echo lang("are_you_sure");?> ?');
                        location.reload();
                    }
                })

});

$('.addQuantityToCart').change(function(e) {
    var book_id = $(this).attr('data-id');
    var quantity = Math.abs($(this).val());
    // $(this).val() = quantity;

        $.ajax({
            type: "POST",   
            url: `/books/setItemQuantity/` + book_id + `/` + quantity,
            dataType: 'json',
            // data: data, 
        }).done(function(data){
            $('.TotalPrice').text(data.updated_items.totalPrice);
            $('#'+book_id+'').text(data.updated_items.totalPriceForItem);
        })

});
</script>

<?php $this->load->view('templates/footer');  ?>
