<?php 
$this->load->view('templates/header'); // APP_PATH.'views/dflwekqdh.php'
?>


<!-- use this for popup-->
<?php if (!isset ( $_SESSION['agree_cookies']) ) :?>
<div id="boxes">
  <div style="top: 199.5px; left: 551.5px; display: none;" id="dialog" class="window">
	Book store
    <div id="lorem">
      <p style="text-align:center"> <?=lang('policy')?> </p>
    </div>
    <div id="popupfoot"> 
		<a href="#" class="close agree">
			<?php echo lang("if_you_do_not_agree");?>
		</a> 

		<a class="agree btn btn-danger" href="/user/agreeCookies">
			<?php echo lang("agree");?>
		</a> 
    </div>
  </div>
  <div style="width: 1478px; font-size: 32pt; color:white; height: 602px; display: none; opacity: 0.8;" id="mask"></div>
</div>
<?php endif ?>
<!-- use this for popup-->

<div>




<?PHP
$page_records_options=array(2,4,8,16);


echo '<div class="result_select"><select name="result">';
foreach($page_records_options as $rec_option){
    $selected='';
    if($rec_option == $recordsPerPage ){
        $selected=' selected ';
    }
    echo '<option '.$selected.' value="'.$rec_option.'">'.$rec_option.' '.lang("pages_text").' </option>';    
}
echo '</select></div>';

$titleActive = '';
$arrowTitleUpActive = '';
$arrowTitleDownActive = '';

$authorActive = '';
$arrowAuthorUpActive = '';
$arrowAuthorDownActive = '';

$priceActive = '';
$arrowPriceUpActive = '';
$arrowPriceDownActive = '';


if($this->uri->segment(6,0) == 'title'){
    $titleActive = 'style = "color: red;"';

    if($this->uri->segment(7,0) == 'asc'){
        $arrowTitleUpActive = 'style = "color: green;"';
    }
    else if($this->uri->segment(7,0) == 'desc'){
        $arrowTitleDownActive = 'style = "color: green;"';
    }
}
if ($this->uri->segment(6,0) == 'author'){
    $authorActive = 'style = "color: red;"';

    if($this->uri->segment(7,0) == 'asc'){
        $arrowAuthorUpActive = 'style = "color: green;"';
    }
    else if($this->uri->segment(7,0) == 'desc'){
        $arrowAuthorDownActive = 'style = "color: green;"';
    }
}

if ($this->uri->segment(6,0) == 'price'){
    $priceActive = 'style = "color: red;"';

    if($this->uri->segment(7,0) == 'asc'){
        $arrowPriceUpActive = 'style = "color: green;"';
    }
    else if($this->uri->segment(7,0) == 'desc'){
        $arrowPriceDownActive = 'style = "color: green;"';
    }
}


    echo '
        <div class="container">
            <div class="row">
                <div class="col-sm-4">  
					<h4 '.$titleActive.'> '.lang("title").'
					
						<a href="/books/page/'.$cat.'/1/'.$recordsPerPage.'/title/asc" style="display: inline-block;">
							<div '.$arrowTitleUpActive.' class="up"> 
								<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span> 
							</div>
						</a>      

						<a href="/books/page/'.$cat.'/1/'.$recordsPerPage.'/title/desc" style="display: inline-block;">
							<div '.$arrowTitleDownActive.' class="down">
								<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
							</div>
						</a> 
											
					</h4>
				</div>

                <div class="col-sm-4">  
					<h4 '.$authorActive.'> '.lang("author_text").'
						<a href="/books/page/'.$cat.'/1/'.$recordsPerPage.'/author/asc" style="display: inline-block;"><div '.$arrowAuthorUpActive.' class="up"> <span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span> </div></a>      
						<a href="/books/page/'.$cat.'/1/'.$recordsPerPage.'/author/desc" style="display: inline-block;"><div '.$arrowAuthorDownActive.' class="down"> <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span> </div></a> 
					</h4> 
                </div>

                <div class="col-sm-4">  
					<h4 '.$priceActive.'> '.lang("price").'
						<a href="/books/page/'.$cat.'/1/'.$recordsPerPage.'/price/asc" style="display: inline-block;">
							<div '.$arrowPriceUpActive.' class="up"> 
							<span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
							</div>
						</a>      
						<a href="/books/page/'.$cat.'/1/'.$recordsPerPage.'/price/desc" style="display: inline-block;">
							<div '.$arrowPriceDownActive.' class="down"> 
								<span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span> 
							</div>
						</a> 
					</h4> 
                </div>


        ';
echo "";
echo '
    <div class="container">
    <div class="row">
';


foreach ($books as $books_item){
        echo '

            <div class="col-md-4">
                <div class="thumbnail">
                    <img src="/assets/images/books/'.$books_item['id'].'/'.$books_item['active_photo'].'" alt="" class="img-responsive" width=400px height=400px>
                        <div class="caption">
                            <h4 class="pull-right">'.$books_item['price'].'  '.lang("currency").'</h4>
                            <h4><a href="#">'.$books_item['title'].'</a></h4>
                            <p>'.substr($books_item['description'], 0, 200) .'</p>
                        </div>
                        
                        <div class="ratings">
                          <p>
                            <span class="glyphicon glyphicon-star">  </span>
                            '.lang("author_text").' : '.$books_item['author'].'
                          </p>
                        </div>
                        <div class="ratings">
                          <p>
                            <span class="glyphicon glyphicon-star"></span>
                            '.lang("remaining_pieces").' : '.$books_item['quantity'].'
                          </p>
                        </div>
                        
                        <div class="space-ten"></div>
                        <div class="btn-ground text-center">
                            <button type="button" class="btn btn-primary add_to_cart" data-id='.$books_item['id'].'><i class="fa fa-shopping-cart"></i> '.lang('addToCart').'</button>
                            <a href="/books/getexactbook/'.$books_item['id'].'">
                                <button type="button" class="btn btn-primary"><i class="fa fa-search"></i> '.lang('quickView').'</button>
                            </a>
                        </div>
                    <div class="space-ten"></div>
                </div> <!-- thumbnail -->
            </div> <!-- col-md-4 -->
';
}
echo '
</div> <!-- div class row -->
</div> <!-- div class container -->
';
            // работи , но с числа по малки от максималния брой страници
$maxShow = 5;
if($maxShow > $maxPages){
    $maxShow = $maxPages;
}

$show_start = 1;

$active = 'class= "active"';

?>
<nav style='text-align:center;'>
        <ul class='pagination'>
                <li>  
                    <a href="<?php echo '/books/page/'.$cat."/1/".$recordsPerPage."/".$ordery_by."/".$ordery_type ?>"> First </a>
                </li>

                <li>
                <a href="<?php echo '/books/page/'.$cat."/".$prevPage."/".$recordsPerPage."/".$ordery_by."/".$ordery_type ?>"> Previous  </a>
                </li>
                <?php
                /**
                 * Ако е една страница или изобщо няма книги 
                 * Да покаже че общо има една страница
                 */

                if($page == $maxPages || empty($books)){
                   $nextPage = $page;
                }
                
                if($page > 0 && $page < $maxPages){
                        $show_start = $page - intval($maxShow/2);
                    
                    if($show_start < 1){
                            $show_start = 1;
                        }
                }
                else
                {
                    $maxShow = ($maxPages >= $maxShow ? $maxShow : $maxPages);
                    $show_start = $maxPages - $maxShow+1;
                }

                if($maxShow == 0){
                    $maxShow = 1;
                }
                for($i=0; $i <= $maxShow-1 ; $i++){                    

                    /**
                     * Използва се за изключване на излишните страници
                     * променливата var е равна на  максималният брой страници - броя страници, които трябва да бъдат изведени
                     * Тоест ще покаже най високия възможен елемент от ляво, началото на пагинацията. 
                     */
                    if($maxPages >= $maxShow){
                            $var = $maxPages - $maxShow;
                            $var = $var+1;
                        
                                if($show_start > $var){
                                    $show_start = $var;
                                }
                    }
                    $my_page = $i + $show_start;
                    if($my_page == $page){
                            echo '
                            <li>
                                <a '.$active.'href="/books/page/'.$cat.'/'.$my_page.'/'.$recordsPerPage."/".$ordery_by."/".$ordery_type.'">'.$my_page.'</a> 
                            </li>
                            ';
                    }
                    else
                    {
                        echo '
                            <li>
                                <a href="/books/page/'.$cat.'/'.$my_page.'/'.$recordsPerPage."/".$ordery_by."/".$ordery_type.'">'.$my_page.'</a>
                            </li>
                        ';
                    }
                }
                
                ?>
                <li>

                <a href="<?php echo "/books/page/".$cat."/".$nextPage."/".$recordsPerPage."/".$ordery_by."/".$ordery_type ?>"> Next </a> 
                </li> 
                <li style="display: none;">
                        <a href="<?php echo "/books/page/".$cat."/".$maxPages."/".$recordsPerPage."/".$ordery_by."/".$ordery_type ?>"> Last </a>
                </li>
        </ul>
</nav>   

<script> type="text/javascript">

$( document ).ready(function() {
    console.log( "ready!" );
    $( "select[name=result]" ).change(function(e) {
        var results = $(this).val();
        window.location.href = '<?php echo "/books/page/".$cat."/1/" ?>'+results+'/<?php echo ''.$ordery_by.'/'.$ordery_type.''; ?>';
    });    
});



$( '.add_to_cart' ).click(function(e) {
    var data = {};
    var book_id = $(this).attr('data-id');

    data.quantity = $('#quantity').val(); 
    data.item = $('.count').val();
    data.itemId = book_id;  
   

        $.ajax({
            type: "POST",   
            url: `/books/setSesssionData/`,
            dataType: 'json',
            data: data,
            
        }).done(function(data) {
            console.log(data.message);
            // alert(JSON.stringify(data.data))
                if (data.data.message == "quantity"){
                    // alert(JSON.stringify(data.data.totalPrice));
                    $('.TotalPrice').text(data.data.totalPrice);
                    $('.count').text(data.data.count);
                    //    alert("Успешно добавихте елемент в количката");
                    //    location.reload();
                 }else if(data.data.message == "new"){
                    //  alert(data.data.message);
                    $('.TotalPrice').text(data.data.totalPrice);
                    $('.count').text(data.data.count);
                 }else{
                    location.reload();
                 }
        })

});

$(document).ready(function() {    

var id = '#dialog';

//Get the screen height and width
var maskHeight = $(document).height();
var maskWidth = $(window).width();

//Set heigth and width to mask to fill up the whole screen
$('#mask').css({'width':maskWidth,'height':maskHeight});

//transition effect		
$('#mask').fadeIn(500);	
$('#mask').fadeTo("slow",0.9);	

//Get the window height and width
var winH = $(window).height();
var winW = $(window).width();
      
//Set the popup window to center
$(id).css('top',  winH/2-$(id).height()/2);
$(id).css('left', winW/2-$(id).width()/2);

//transition effect
$(id).fadeIn(2000); 	

//if close button is clicked
$('.window .close').click(function (e) {
//Cancel the link behavior
e.preventDefault();

$('#mask').hide();
$('.window').hide();
});		

//if mask is clicked
$('#mask').click(function () {
$(this).hide();
$('.window').hide();
});		

});
</script>
<?php $this->load->view('templates/footer');  ?>

 
