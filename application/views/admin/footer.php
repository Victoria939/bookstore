<footer class="footer">
	<div class="container">
		<div class="footer__text">
			<p>
				Bookstore &copy; <?php echo date("Y"); ?>
			</p>
		</div><!-- /.footer__text -->
	</div><!-- /.container -->
</footer>