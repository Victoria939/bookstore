﻿<?php $this->load->view('templates/header');  

// value na phonenumber
// var_dump($adress);
if($isLogged == true){
  $name = 'value="'.$userInfo[0]["username"].'", disabled="disabled"';
  $familyName = 'value="'.$userInfo[0]["usrFamName"].'", disabled="disabled"';
  $email = 'value="'.$userInfo[0]["email"].'", disabled="disabled"';
  $chooseCity = $adress;
  $chooseOffice = $office;
  $phoneNumber = 'value="'.$userInfo[0]["phoneNumber"].'", disabled="disabled"';
  $adress  = 'value="'.$adress.'", disabled="disabled"';
  $city = 'value="'.$city.'", disabled="disabled"';
  $office = 'value="'.$office.'", disabled="disabled"';
  
}else{
$name = '';
$familyName = '';
$email = '';
$chooseCity = '';
$chooseOffice = '';
$phoneNumber = 'value = "+359 "';
$adress  = '';
$city = '';
$office = '';
}


?>

<?php 

$attributes = array('role'=>"form", 'id'=>"createTempRegister");
echo form_open('register/orderName', $attributes); 

?>

<div class="container">
  <div class="form-row">
    <div class="form-group">
      <label for="name"><?php echo lang("name");?></label>
      <input type="text" class="form-control" name="name" id="name" placeholder="<?php echo lang("name");?>" <?=$name?>>
    </div>


    <div class="form-group">
      <label for="familyName"><?php echo lang("famName");?></label>
      <input type="text" class="form-control" name="familyName" id="familyName" placeholder="<?php echo lang('famName');?>" <?=$familyName?> >
    </div>


    <div class="form-group">
      <label for="Email">Email</label>
      <input type="text" class="form-control" name="txtEmail" id="txtEmail" placeholder="Email" <?=$email?> >
    </div>


    <div class="form-group">
      <label for="number"><?php echo lang("phone");?></label>
      <!-- <div class="input-group"> -->
        <!-- <span class="input-group-addon" id="basic-addon1">+359</span> -->
        <input type="text" class="form-control" name="number" <?=$phoneNumber?>  id="number">
      <!-- </div> -->
    </div>


    <div class="form-group">
      <label for="adress"><?php echo lang("adress");?></label>
      <input  <?=$adress?> type="text" class="form-control" name="adress" id="adress" placeholder="<?php echo lang("adress");?>">
    </div>


    <div class="form-group col-md-6">
      <label for="city"><?php echo lang("city");?></label>
        <select  <?=$city?> name="chooseCity" id="chooseCity" class="form-control">
          <option value="choose"  disabled selected> <?=$chooseCity?></option>
          <option value="Sofia"><?php echo lang("city_sofia");?></option>
          <option value="Burgas"><?php echo lang("city_bourgas");?></option>
          <option value="Varna"><?php echo lang("city_varna");?></option>
          <option value="StaraZagora"><?php echo lang("city_stara_zagora");?></option>
        </select>
    </div>
    
    <div name="chooseCity" id="chooseCity" class="form-group col-md-6">
      <label <?=$office?> for="Office"><?php echo lang("office");?></label>
      <select name="selectOffice" id="selectOffice" class="form-control" disabled="disabled">
      <option value="choose"  disabled selected> <?=$chooseOffice?></option>
        </select>
    </div>


    <div class="form-group">
      <label for="comment"><?php echo lang("comment_on_the_order");?></label>
      <input type="text" class="form-control" name="comment" id="comment" placeholder="<?php echo lang('comment_on_the_order');?>">
    </div>


    <div class="form-group col-md-6">
        <input type="submit" id="saveCart" class="btn btn-success center-block" value="<?php echo lang('complete_the_order');?>">
    </div>


    <div class="form-group col-md-6">
      <input type="button" id="emptyCart" class="btn btn-danger center-block" value="<?php echo lang('clear_the_card');?>">
    </div>
  </div>
  <?php echo form_close(); ?>

<script type="text/javascript">    
  $('#emptyCart').on('click', function() {
      $.ajax({
          url: '/BuyItemsInCart/clearCard',
          method: 'POST',
          // data: data
      })
      .done(function(status) {
          if (status == "deleted"){
            // alert(data.message);
              window.location.href = "http://bookstore.local/books/page/33/1/2/title/asc";
          }
          else{
            window.location.href = "http://bookstore.local/books/page/33/1/2/title/asc";
          }
        })
  })

  

$('#chooseCity').change(function(e){
  // if($('#createTempRegister').valid() == true){
  $('#selectOffice').attr("value",'').text('');
  var choosedCity = $(this).val();
    e.preventDefault()
      $.ajax({
        url: '/BuyItemsInCart/chooseShop/'+choosedCity,
        method: 'POST',
        dataType: 'json',
        data: choosedCity,
      }).done(function(data){
            if(typeof data.shopArr != 'undefined')
            {
              $.each(data.shopArr,function(key,value)
              {
                $('#selectOffice').prop("disabled", false);
                $('#selectOffice')
                .append($("<option></option>")
                    .attr("value",value)
                    .text(value)); 
              });
            }
            else
            {
              alert(typeof data.shopArr);
              alert('<?php echo lang("error_msg");?>');
            }
      })
// }
// else{ return false;}
})


/**
Запазване на информацията от формата и предаването на даните
*/

  // if($('#createTempRegister').valid() == true){
    $('#saveCart').on('click', function(e){
      e.preventDefault();
      var data_form = $('#createTempRegister').serialize();
      
        $.ajax({
          url: '/BuyItemsInCart/saveInformation/',
          method : 'POST',
          dataType: 'json',
          data: data_form,
        }).done(function(data){
          /**
          * Проверява дали формата е попълнена правилно
          * и тогава продължава с изпълнението на поръчката
          */
          if($('#createTempRegister').valid() == true){
              if(data.success == true){
                   swal({title: "<?php echo lang('the_order_is_completed');?>", type: "success"});
                   setTimeout(function () {
                             window.location.href = "http://bookstore.local/books/page/33/1/2/title/asc";
                                }, 3000);
              }else if(data.error == "<?php echo lang('cartEmpty');?>" ){
                  swal({title: data.error, type: "error"});
                  setTimeout(function () {
                             window.location.href = "http://bookstore.local/books/page/33/1/2/title/asc";
                                }, 3000);
              }
              else{
                swal({title: data.error, type: "error"});
               }
          }
          
        }).fail(function() {    
                    return false;
                });

    })
// }else{
  // return false;
// }


</script>

<?php $this->load->view('templates/footer');  ?>
