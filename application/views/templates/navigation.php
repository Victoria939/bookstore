<?php 
$activeCategories = '';
$activeCreate = '';
$printSessionItems = '';
$registerUser = '';
$userLogin = '';
$profile = '';
$changepassword = '';
$adress = '';
$photo = '';
$profileInfo = '';
$activeAdmin = '';
$adminCategories = '';


if($this->uri->segment(2) == 'page'){ 
    $activeCategories="active"; 
}else if ($this->uri->segment(2) == 'register'){
    $registerUser = "active";
}else if ($this->uri->segment(2) == 'login'){
    $userLogin = "active";
// }else if ($this->uri->segment(1) == 'user'){
    // $profile = "active";
}else if($this->uri->segment(2) == 'changepassword'){
    $changepassword = "active";
    $profile = "active";
}else if($this->uri->segment(2) == 'adress'){
    $adress = "active";
    $profile = "active";
}else if($this->uri->segment(2) == 'photo'){
    $photo = "active";
    $profile = "active";
}else if($this->uri->segment(2) == 'info'){
    $profileInfo = "active";
    $profile = "active";
}else if($this->uri->segment(1) == 'books' && $this->uri->segment(2) == 'create'){
    $activeAdmin = "active";
}else if($this->uri->segment(1) == 'category' && $this->uri->segment(2) == 'createCategory'){
    $adminCategories = "active";
}else{
    $printSessionItems = "active";
}
?>
<div id="main-menu">
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <ul class="nav navbar-nav navbar">
        
    
    <li class="dropdown <?=$activeCategories?>"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?=lang('books')?> <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <?php

					if (!empty($category)) {
						$titles = $category['title'];
						$categoryIds = $category['itemId'];
							foreach ($titles as $value => $title){
								$URL_Segment = $this->uri->segment(3, 0);
								if($URL_Segment === $title['id']){
									echo '<li class="active"> <a href="/books/page/'.$title['id'].'/1/2/title/asc"> '.$title['title'].' '.$categoryIds[$value].' </a></li>';
								}
								else{
									echo '<li> <a href="/books/page/'.$title['id'].'/1/2/title/asc"> '.$title['title'].' '.$categoryIds[$value].' </a></li>';
								}
								
								
							}
					}
                ?>  
            </ul>
      </li>

      
    <li class="<?=$printSessionItems?>">
        <a href="/books/printSessionItems"> 
            <span class="glyphicon glyphicon-shopping-cart"></span>  
                    <?php
                        if($count == 1){
                            echo '<span class="count">'.$count.'</span> '.lang('article').' <span class="TotalPrice">' .$totalPrice.'</span> '.lang('currency').' ';
                        }
                        else if($count>1){
                            echo '<span class="count">'.$count.'</span> '.lang('articles').' <span class="TotalPrice">' .$totalPrice.'</span> '.lang('currency').' ';
                        }else{
                            echo lang('cartEmpty');
                        }
                    ?>   
            </a>
        </li>


    <!-- $this->session->set_userdata('site_lang','sd'); -->

    <?php if ((isset($_SESSION['userInfo']['username'])) && ($_SESSION['userInfo']['username'] === 'admin')) : ?>
        <li class='dropdown'><a class="dropdown-toggle" data-toggle="dropdown" href="#"> <?=lang('admin')?> <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="/books/create/"><?=lang('addBook')?></a></li>
                <li><a href="/category/createCategory"><?=lang('createCategory')?></a></li> 
                <li><a href="/orders/getNotActiveOrders"><?=lang('nonActiveOrders')?></a></li> 
                <!-- $this->session->set_userdata('site_lang','sd'); -->
            </ul>
        </li>
    <?php endif ?>

<li class='dropdown'><a class="dropdown-toggle" data-toggle="dropdown" href="#"> <?=lang('lang')?> <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li><a href="/LanguageSwitcher/switchLang/english"><?=lang('en')?></a></li>
        <li><a href="/LanguageSwitcher/switchLang/bulgarian"><?=lang('bg')?></a></li> 
        <!-- $this->session->set_userdata('site_lang','sd'); -->
    </ul>
</li>


        

</ul>

<?php
if ($isLogged == true){
?> 
<ul style="float: right" class="nav navbar-nav navbar-right">
    <!-- <li class=<?=$profile?> >  <a href="/user/profile"><span class="glyphicon glyphicon-user"></span> Профил</a></li> -->

    <li class="dropdown <?=$profile?>"><a class="dropdown-toggle" data-toggle="dropdown" href="/user/profile/profile"><span class="glyphicon glyphicon-user"></span> <?php echo $currUserName ?><span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li class=<?=$profileInfo?>> <a href="/user/info">              <?=lang('editInfo')?></a></li>
                <li class=<?=$changepassword?> > <a href="/user/changepassword"><?=lang('changePassword')?></a></li>
                <li class=<?=$photo?>> <a href="/user/photo">                   <?=lang('changeProfilePhoto')?> </a></li>
                <li class=<?=$adress?> > <a href="/user/adress">                <?=lang('deliveryAdresses')?> </a></li>
				<li style="display: none;" class=<?=$adress?> > <a href="/user/subscribe">             <?=lang('subscribe')?> </a></li>
            </ul>
    </li>
       
    
    <li> <a href="/user/logout"><span class="glyphicon glyphicon-log-in"></span> <?=lang('exit')?></a></li>


<?php }else{ ?>
<ul style="float: right" class="nav navbar-nav navbar-right">
    <li class=<?=$registerUser?> >  <a href="/user/register"><span class="glyphicon glyphicon-user"></span> <?=lang('register')?></a></li>
    <li class=<?=$userLogin?> >     <a href="/user/login"><span class="glyphicon glyphicon-log-in"></span> <?=lang('login')?></a></li>
</ul>

<?php } ?>        
  </div>
</nav>
</div>




