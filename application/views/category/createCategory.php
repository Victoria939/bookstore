<?php $this->load->view('templates/header');  ?>
<?php echo form_open('category/createCategory'); ?>
<?php echo validation_errors(); ?>

<div class="container">
    <form action="/category/createtagory" method=POST ?>
        <div class="row">
            <div class="col-sm-1 col-sm-offset-4">  
                <label for="prio"><?=lang('prio')?></label> 
            </div>
            <div class="col-sm-4">  
                <input type="number" name="Prio" /> 
            </div>
        </div>

        <div class="row">
            <div class="col-sm-1 col-sm-offset-4">  
                <label for="title"><?=lang('title')?></label> 
            </div>
            <div class="col-sm-4">  
                <input type="text" name="Title" /> 
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-1 col-sm-offset-4">  
                <label for="prio"><?=lang('keyword')?></label> 
            </div>
            <div class="col-sm-4">  
                <input type="text" name="Keyword" /> 
            </div>
        </div>
        
        
        <!-- <div class="col-sm-12 col-sm-offset-4">   <input type="text" name="Title"/> </div>
        <div class="col-sm-12 col-sm-offset-4">  <label for="keyword">Keyword</label> <input type="text" name="Keyword"/> </div> -->

        <div class="row" style="margin:25px 0px 0px 100px">
            <div class="col-sm-12 col-sm-offset-4">
                <input type="submit" name="submit" value="<?=lang('create_new_cat')?>" />
            </div>
        </div>
    </form>
</div>
<?php $this->load->view('templates/footer');  ?>