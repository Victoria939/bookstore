<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('permisions'))
{
	function permisions($my_permissions, $permissions_required_arr){  
		if($my_permissions == USER_LEVEL_SUPERADMIN) return true; // WELCOME MASTER			
	
		if(	!in_array($my_permissions,$permissions_required_arr) and !in_array(USER_LEVEL_FREE,$permissions_required_arr)){ // PERMISSIONS
			return false;
		}				
		return true;
	}
}


/* End of file alpha_helper.php */
/* Location: ./system/helpers/alpha_helper.php */
