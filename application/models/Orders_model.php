<?php

class Orders_model extends CI_Model {

      
    public function __construct(){
        $this->load->database();
    }
    
    /**
     * Вземна на всички поръчки очакващи 
     * активация
     * @return Array , съдържа всички записи(поръчки) очакващи удобрение
     */
    public function getActiveOrders(){
        // $getNonActiveOrdersQuery = $this->db->get_where('st_order_product', array('order_status' => "pending_order"), $limit, $offset);
        $getNonActiveOrdersQuery =  $this->db->select('*')
                                             ->where('order_status', 'pending_order')
                                             ->get('st_order_product')
                                             ->result();
        
        //Проверява дали има подходящи резултати
        $allResults = $this->db->count_all_results();
        if($allResults < 1){
           return false;
        }

       
        $nonActiveRecords_array = array();

        foreach($getNonActiveOrdersQuery as $item){
            $nonActiveRecords_array[]= array(
                                    'id'            => $item->id,
                                    'createdBy'     => $item->createdBy,
                                    'productId'     => $item->productId,
                                    'quantity'      => $item->quantity,
                                    'totalPrice'    => $item->totalPrice,
                                    'dateAdded'     => $item->dateAdded,
                                    'order_status'  => $item->order_status,
                                    'orders'        => json_decode($item->items, true)
                                    );
        }
        return $nonActiveRecords_array;
    }

    /**
     * Вземна на всички поръчки очакващи 
     * активация
     * @return Array , съдържа всички записи(поръчки) очакващи удобрение
     */
    public function getNonActiveOrders(){
        // $getNonActiveOrdersQuery = $this->db->get_where('st_order_product', array('order_status' => "pending_order"), $limit, $offset);
        $getNonActiveOrdersQuery =  $this->db->select('*')
                                             ->where('order_status','sended')
                                             ->get('st_order_product')
                                             ->result();
        
        //Проверява дали има подходящи резултати
        $allResults = $this->db->count_all_results();
        if($allResults < 1){
           return false;
        }

       
        $nonActiveRecords_array = array();

        foreach($getNonActiveOrdersQuery as $item){
            $nonActiveRecords_array[]= array(
                                    'id'            => $item->id,
                                    'createdBy'     => $item->createdBy,
                                    'productId'     => $item->productId,
                                    'quantity'      => $item->quantity,
                                    'totalPrice'    => $item->totalPrice,
                                    'dateAdded'     => $item->dateAdded,
                                    'order_status'  => $item->order_status,
                                    'orders'        => json_decode($item->items, true)
                                    );
        }
        return $nonActiveRecords_array;
    }


    /**
    *Информация за потребителя
    *@param id на потребителя направил поръчката
    *@return Array с информация за потребителя по зададено ID
    *@access public
    */
    public function getUser($id){
        // echo $id.'<br>';
        $getUser =  $this->db->select('*')
                             ->where('id',$id)
                             ->get('st_order_customer')
                             ->result();

        $getUserResult = $this->db->count_all_results();
        if($getUserResult < 1){
            return false;
        }

        foreach ($getUser as $info){
            $userInfo = array(
                                    'id'                => $info->id,
                                    'Name'              => $info->Name,
                                    'FamilyName'        => $info->FamilyName,
                                    'Mail'              => $info->Mail,
                                    'PhoneNumber'       => $info->PhoneNumber,
                                    'Address'           => $info->Address,
                                    'Comment'           => $info->Comment,
                                    'office'            => $info->office,
                                    'city'              => $info->city,
                                    'TimeAddedOrder'    => $info->Time,
                                    'totalPrice'        => $info->totalPrice,
                                    );
        }
    
        return $userInfo;
    }

     /**
    *Информация за поръчката
    *@param id на потребителя направил поръчката
    *@return Array с информация за потребителя по зададено ID
    *@access public
    */
    public function getProduct($id){
        // echo $id.'<br>';
        $getProduct =  $this->db->select('*')
                                             ->where('id',$id)
                                             ->get('book')
                                             ->result();

        $getProductResult = $this->db->count_all_results();
        if($getProductResult < 1){
            return false;
        }

        foreach ($getProduct as $info){
            $productInfo = array(
                                    'id'                => $info->id,
                                    'title'             => $info->title,
                                    'price'             => $info->price,
                                    'quantity'          => $info->quantity,
                                    'active_photo'      => $info->active_photo,
                                    );
        }
    
        return $productInfo;
    }

    public function completeOrder( $id = "", $quantity = "", $orderId = ""){
        $currentQuantityArr = $this->db->select('*')
                                            ->where('id',$id)
                                            ->get('book')
                                            ->result();

        $currQuantity = $currentQuantityArr[0]->quantity;
        $updatedQuantity = $currQuantity - $quantity;

        $updateData = array(
            'quantity' => $updatedQuantity,
         );

         $this->db->where('id', $id);
         $this->db->update('book', $updateData);

         $this->db->where('id', $orderId);
         $this->db->update('st_order_product', ['order_status' => 'sended' ]);  


    }


}
?>
