<?php

class Books_model extends CI_Model {
    //Limit on page
    private $limit=2; //private $var $limit

    public function __construct(){
        $this->load->database();
    }

    //Get book 
    //Example
    //Page 3
    //Begin at 15 - 20
    public function get_books($cat_id,$page){
        $start=($page*$this->limit)-1;
        return  $start; //$this->getBooksByCategory($cat_id,$start,$this->limit);
    }

     //Как да взема стойността на limit 
     //Взима всички Имена на книги, автори, описание, цена
     // category = 33
     //page = 1 
     //cnt = 1 
     public function getBooksByCategory($cat_id,$page,$limit,$order_by, $order_type, $get_total=true){     
        // echo $limit;  
        // var_dump($limit);
        if($page == 1)
        {
            $start = 0;
        }
        else
        {
            $start=($page*$limit)-$limit;
        }

        $sql = "SELECT book.id, book.title, book.author, book.description, book.price ,book.quantity, book.active_photo ";
        $crit = "FROM book 
        LEFT JOIN category_book_links 
        ON category_book_links.object_id = book.id 
        WHERE category_book_links.external_id = ? AND quantity > 0";            
        // GROUP BY book.id "; ще го добавя в последствие
        
        $sql .= $crit;

        if(!empty($order_by) && !empty($order_type)){

            $orderSQl = ' ORDER BY book.'.mysqli_real_escape_string($this->db->conn_id,$order_by).' '.mysqli_real_escape_string($this->db->conn_id,$order_type).'';
            $sql .= $orderSQl;
        }
        $sql .= ' LIMIT ?, ? ';
        // избери всичко от book където category_book_links.object_id = book.id
        //                                  category_book_links.external_id = books_category_id

        //SQL -> Взима тайтъла Автора описанието и цената
        //crit -> Взима само тези, за които е изпълнено да са от дадена категория
        $maxPages=0;
        if($get_total){
            $sql_total = "SELECT COUNT(book.id) as cnt "; //select COUNT(id) as cnt 
            $sql_total .= $crit;
            $query_total =  $this->db->query($sql_total, array($cat_id));     
            // var_dump($sql_total);             
            $total_row=$query_total->result_array();
            
            foreach($total_row as $total_row_item)
            {
                $total_rows = $total_row_item;
            }
        }
        $maxPages = round($total_rows['cnt'] / $limit);
        $limit = (int)$limit;

        
        $query =  $this->db->query  ($sql, array(
                                                    $cat_id,
                                                    $start,
                                                    $limit)
                                        ); // Да избере само тези, които отговарят на категорията
 

            $result=array();
            $result['maxPages']=$maxPages;
            $result['records']=$query->result_array();    

            return $result;
            
    }

    /**
     * Връща активната картинка за даден артикул
     * Ще се използва за виазулизирането
     * param 
     * return
     * 
     */
    public function getActiveBookPhoto($bookId){
        $getBookPhotoSQL = "SELECT `books_photo_cropped_photo` FROM `books_photo` WHERE photo_id=?";  
        $getBookPhotoQuerry =  $this->db->query($getBookPhotoSQL, array($bookId)); 
        echo '<pre>'. print_r($getBookPhotoQuerry, true) .'</pre>';exit();    

    }



    /*
    * Nai nakraq trqbva da dobavq ajax validaciq s jSon za unikalnost na imeto
    *
    *
    *
    *
    */


    
    public function set_book($insert_arr){
        // echo '<pre>'. print_r($insert_arr, true) .'</pre>'; exit;s
        $data = array(
            'title' => $insert_arr['title'],
            'author' =>  $insert_arr['author'],
            'description' =>  $insert_arr['description'],
            'price' =>  $insert_arr['price'],
            'date' => $insert_arr['date'],
            'quantity' => $insert_arr['quantity'],
        );
        $sqlInsert = "INSERT INTO book (title,author,description,price,quantity,date) VALUES (?,?,?,?,?,?);";
        $query =  $this->db->insert('book',$data);         
    }


    public function get_exact_book($id = NULL){
        $sql = "SELECT id, title, author, description, price, quantity, active_photo FROM book where id = ?";
        $query_total =  $this->db->query($sql, array($id));
        $allBooks = $query_total->result_array();

        $getCategorySql = "SELECT external_id FROM category_book_links WHERE object_id=".$id."";
        $getCategoryQuerry =  $this->db->query($getCategorySql);
        $currentCategory = $getCategoryQuerry->result_array();
        $allBooks['currentCategory'] = $currentCategory[0]['external_id'];
        
        return $allBooks;
    }


    public function set_book_category($bookId,$categoryId){
        $data = array(
            'object_id' => $bookId,
            'external_id' => $categoryId,
        );
        return $this->db->insert('category_book_links',$data);
    }

    public function get_categories(){
        // $getCategoryItems
        $getCategoryQuerry = $this->db->get('category_books');
        $getCategory['category'] = $getCategoryQuerry->result_array();
        foreach ($getCategory['category'] as $category_item_id_array){
            $sqlId = $category_item_id_array['id'];
            $sql = "SELECT COUNT(book.id) AS TotalRows FROM book
                    LEFT JOIN category_book_links 
                    ON category_book_links.object_id = book.id 
                    WHERE category_book_links.external_id = '$sqlId'
                    AND book.quantity > 0";
            // echo $sql.'</br>';
            $sqlQuerry =  $this->db->query($sql);
            $itemsPercategory = $sqlQuerry->result_array();
            // var_dump($itemsPercategory[0]['TotalRows']);
            $getCategory['category_item_id'][] = $itemsPercategory[0]['TotalRows'];
            // var_dump($getCategory['category_item_id']);
       } 
        // var_dump($getCategory['category'][2]['title']);
        return  $getCategory;
    }


    public function get_total_records(){
        $totalRecordsSql = "SELECT COUNT(id) FROM book";
        $query = $this->db->query($totalRecordsSql);

        if($query->result_array() > 0 ){
            return $query->result_array();
        }
        else{
            echo 'asd';
        }
    }
    
    public function getPriceById($id){
        if(empty($id)){
           echo 'DIDNT FIDNT ID IN getPriceById </br>';
        }
        $itemPriceSql = "SELECT price from book where id = ?";
        $itemPrice = $this->db->query($itemPriceSql, $id);
        $price = $itemPrice->result_array();
        return $price;
    }


    /**
    *param ptohotId
    *param originalPhoto
    *param croppedPhoto
    *param isActive
    *
    *retrurn boolean
    *access public
    */
    public function setBookPhoto($photoId, $originalPhoto, $croppedPhoto){
         
        if (empty($photoId) || empty($originalPhoto) || empty($croppedPhoto)){
             return false;
         }

         if($this->getActivePhoto($photoId)){
            $setBookPhotoSQL = "INSERT INTO `books_photo` (`photo_id`, `original_photo`, `cropped_photo`, `is_active`) VALUES (?, ?, ?, '0')";
            $updateBookSQL = "UPDATE `book` SET `active_photo` = ? WHERE `book`.`id` = ?";
         }else{
            $setBookPhotoSQL = "INSERT INTO `books_photo` (`photo_id`, `original_photo`, `cropped_photo`, `is_active`) VALUES (?, ?, ?, '1')";
            $updateBookSQL = "UPDATE `book` SET `active_photo` = ? WHERE `book`.`id` = ?";
         }
         $setBookPhotoQuery = $this->db->query($setBookPhotoSQL, array($photoId, $originalPhoto, $croppedPhoto));
         $updateBookQUerry = $this->db->query($updateBookSQL, array($croppedPhoto, $photoId));

         if($setBookPhotoQuery &&  $updateBookQUerry){
             return true;
         }else{
             return false;
         }
    }
     

    /**
    *
    *param photoId 
    *return Bool
    *access public
    */
    public function getActivePhoto($photoId){
        $getActivePhotoSql = "SELECT id FROM `books_photo` WHERE is_active = 1 AND photo_id = ?";
        $getActivePhotoQuery = $this->db->query($getActivePhotoSql, array($photoId));
        $activePhoto = $getActivePhotoQuery->result();

       
        if(empty($activePhoto)){
            return false;
        }
        else{
            return true;

        }
    }


	public function getAllBooks(){
		return $this->db->query('SELECT * FROM book')->result_array();
	}

}
