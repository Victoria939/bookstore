<?php

class Orders extends CI_Controller{

    public $cartItemCount;
    public $category;
    public $totalPrice;
    public $isLogged;
    public $getUsername;
    public $userStatus;

public function __construct(){
    
    parent::__construct();
    
    $this->load->library('session');
    $this->load->model('cart_model');
    $this->load->model('books_model');
    $this->load->model('user_model');
    $this->load->model('orders_model');
    $this->load->helper('url_helper');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->totalPrice = $this ->cart_model->getTotalPrice();
    $this->cartItemCount = $this->cart_model->getSessionQuantityData();
    $this->category = $this->getCategories();
    $this->isLogged = $this->user_model->isLogged();
    $this->getUsername = $this->user_model->getUsername();
    $this->userStatus = $this->user_model->userStatus();


}

    public function index(){
        $category['totalPrice'] = $this->totalPrice;
        $category['category'] = $this->category;
        $category['count'] = $this->cartItemCount;
        $category['isLogged'] = $this->isLogged;
        $category['currUserName'] = $this->getUsername;
        $category['userStatus'] = $this->userStatus;
        $category['orders'] = [];

        $this->load->view('orders/orders.php', $category);
    }

    //Вземана на информация за всички поръчки,
    //които не са активни
    public function getNotActiveOrders(){
        $category['nonActiveOrders'] = $this->orders_model->getNonActiveOrders();

        $nonActiveOrders = $this->orders_model->getNonActiveOrders();
        // echo '<pre>'. print_r($nonActiveOrders, true) .'</pre>'; exit; 

        $nonActiveOrders_array = array();
        foreach ($nonActiveOrders as $order){
            //Вземане на информацията за потребителя
            $productAddedBy  = $this->getUserInfo($order['createdBy']);
            $productInfo     = $this->getProduct($order['productId']);
            $nonActiveOrders_array[] = array(
                                    'id'            => $order['id'],
                                    'userInfo'      => $productAddedBy,
                                    'productInfo'   => $productInfo,
                                    'quantity'      => $order['quantity'],
                                    'totalPrice'    => $order['totalPrice']
                                    );
        }

        // echo '<pre>'. print_r($nonActiveOrders_array, true) .'</pre>'; exit;



        $category['orders'] = $nonActiveOrders_array;
        $category['totalPrice'] = $this->totalPrice;
        $category['category'] = $this->category;
        $category['count'] = $this->cartItemCount;
        $category['isLogged'] = $this->isLogged;
        $category['currUserName'] = $this->getUsername;
        $category['userStatus'] = $this->userStatus;
        $this->load->view('orders/orders.php', $category);
    }

    public function getUserInfo($id){
        return $this->orders_model->getUser($id);
    }

    public function getProduct($id){
        return $this->orders_model->getProduct($id);
    }

    public function getCategories(){
        $categories = $this->books_model->get_categories();
        foreach($categories['category'] as $item){
            $categoryTitle['title'][] = $item;
        }
        foreach($categories['category_item_id'] as $itemId){
            $categoryTitle['itemId'][] = $itemId;
        }
        return $categoryTitle;
    }


    public function completeOrder(){
        $orderId = $_POST['orderId'];
        $bookId = $_POST['itemId'];
        $quantity = $_POST['quantity'];
        $nonActiveOrders = $this->orders_model->completeOrder($bookId, $quantity, $orderId);
        redirect ('orders/getNotActiveOrders');
        exit();
    }
    

}
