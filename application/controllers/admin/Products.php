<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends MY_Controller {

	var $user_permissions = array (USER_LEVEL_SUPERADMIN);
	var $modul  =	'categores';
	 
	function __construct(){	
		parent::__construct();
		$this->load->model('Books_model', '', TRUE);

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->data['products'] = $this->Books_model->getAllBooks();
		$this->data['categories'] = $this->Books_model->get_categories();
		$this->load->view('admin/admin_products_list', $this->data);	
	}

	public function create()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->data['products'] = $this->Books_model->getAllBooks();
		$this->data['categories'] = $this->Books_model->get_categories();
		$categoryId = $this->input->post('category', true);

		

		// var_dump($_POST);
		$insert_arr['title'] 		= trim($_POST['title']);
		$insert_arr['author'] 		= trim($_POST['author']);
		$insert_arr['description'] 	= trim($_POST['description']);
		$insert_arr['price'] 		= trim($_POST['price']);
		$insert_arr['category'] 	= trim($_POST['category']);
		$insert_arr['date'] 		= trim(date('Y-m-d H:i:s'));
		$insert_arr['quantity']  	= $this->input->post('quantity', true);



		$this->Books_model->set_book($insert_arr);
		$bookId = $this->db->insert_id();
		$this->Books_model->set_book_category($bookId, $categoryId);
		redirect(base_url() . "admin/products/setBookPhoto/" . $bookId);

	}


	/**
	 * Взема id-to на създадената книга
	 * записва я и я кропва до квадрат
	 * 
	 * 
	 */
	public function setBookPhoto($id){
		/**
		 * Създава се папка с ID за конкретния артикул
		 */
		if (empty($id)) {
			return false;
		}

		if (!is_dir('./assets/images/books/' . $id)) {
			mkdir('./assets/images/books/' . $id, 0777, true);
		}

		if (!empty($_FILES['bookFile'])) {
			$originalName = pathinfo($_FILES['bookFile']['name'], PATHINFO_FILENAME);
			$originalNameExtension = $_FILES['bookFile']['name'];
			$extension = pathinfo($originalNameExtension, PATHINFO_EXTENSION);

			/**
			 * Какво може да се добавя 
			 * задават се лимитите
			 */
			$config['upload_path']          = ("./assets/images/books/" . $id);
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 2048;
			$config['max_width']            = 5000;
			$config['max_height']           = 5000;
			$config['file_name']            = $originalNameExtension;




			$this->load->library('upload', $config);
			$this->upload->initialize($config);


			/**
			 * Не запазва файла, а дава информация за файла, който ще се качва
			 */
			$upload_data = $this->upload->data();
			// $_FILES['file']['name'];



			/**
			 * Взимане на височината и дължината на картинката
			 */
			$image_info = getimagesize($_FILES["bookFile"]["tmp_name"]);
			$width = $image_info[0];
			$height = $image_info[1];



			$uploadFile = $this->upload->do_upload('bookFile');
			// Качване на оригиналния файл
			if (!$uploadFile) {
				$error = array('error' => $this->upload->display_errors());
			}



			/**
			 * Проверка дали файла е качен (оригиналния и ако да )
			 * прави проверка коя от страните е по - малка за да ресайзне по нея
			 * библиотеката gd2 работи с resize и crop
			 * ако реша в последствие да resize снимката просто мога да задам условие
			 *  $this->image_lib->resize();
			 * със същите параметри
			 */
			if ($uploadFile) {

				$config['image_library'] = 'gd2';
				$config['source_image'] = "./assets/images/books/" . $id . "/" . $originalNameExtension;
				$config['create_thumb'] = TRUE;
				$config['thumb_marker'] = '_cropped';
				$config['maintain_ratio'] = FALSE;

				// echo 's';
				// var_dump($config['source_image']);exit();

				if ($width >= $height) {
					$config['width']        = $height;
					$config['height']       = $height;
				} else {
					$config['width']        = $width;
					$config['height']       = $width;
				}


				$this->load->library('image_lib', $config);
				$isImageCropped = $this->image_lib->crop();

				if ($isImageCropped) {
					$croppedName = $originalName . '_cropped';
					$imageInfo = array(
						'userId'        => $id,
						'originalName'  => $originalName . '.' . $extension,
						'croppedImage'  => $croppedName . '.' . $extension,
					);


					$setBookPhoto = $this->Books_model->setBookPhoto($id, $originalName . '.' . $extension, $croppedName . '.' . $extension);

					if ($setBookPhoto) {
						return $imageInfo;
					} else {
						return false;
					}
				}
			}

		}

		$this->data['id'] = $id;         
		$this->load->view('admin/admin_set_book_photo', $this->data);	

	}

	
}

