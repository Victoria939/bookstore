<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends MY_Controller {

	var $user_permissions = array (USER_LEVEL_SUPERADMIN);
	var $modul  =	'categores';
	 
	function __construct(){	
		parent::__construct();
		$this->load->model('Category_model', '', TRUE);

		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->data['categories'] = $this->Category_model->get_category();
		$this->load->view('admin/admin_categories_list', $this->data);	
	}

	public function createCategory()
	{
		

		$this->form_validation->set_rules('Prio', 'Prio', 'required|integer|max_length[11]');
		$this->form_validation->set_rules('Title', 'Title', 'required|min_length[5]|max_length[255]|is_unique[category_books.title]');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'required|min_length[5]|max_length[255]');


		if ($this->form_validation->run() !== FALSE) {
			$this->Category_model->set_category();
		}
		

		$this->data['categories'] = $this->Category_model->get_category();
		$this->load->view('admin/admin_categories_list', $this->data);
	}

	
}

