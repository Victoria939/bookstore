<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	var $user_permissions = array(USER_LEVEL_FREE,USER_LEVEL_REGISTERED,USER_LEVEL_SUPERADMIN); // USER_LEVEL_SUPERADMIN,USER_LEVEL_ADMIN,USER_LEVEL_REGISTERED,USER_LEVEL_FREE
	var $user_info;
	var $page_type = PAGE_TYPE_FRONT ;	
	var $language='en';
	var	$data;
	var	$session_id;
	var	$av_config;
	var	$av_admin_config;	
	var $av_settings_config;
	var $is_ajax=false;
	var $work_start;
	var $work_end;
	var $site_off;
	var $out_of_work = false;
	var $device = "desktop"; // device type - mobile, tablet, desktop
	var $site_city_id = 'sofia'; // this is for new feature called Restaurant prices
	var $pre_lang;
	var $restaurant_object = null;
	var $global_not_working_time = 10;
	var $today_discount_categories = array();
	var $restaurant_indicators = array();
	var $my_page = false;
	
	function __construct(){
		parent::__construct();	
	
		date_default_timezone_set("Europe/Sofia");
		// Enable errors in dev
		// if( strpos($_SERVER['HTTP_HOST'],'redesign.aladinfoods.bg') !== false ) {
		// 	error_reporting(E_ALL);
		// 	ini_set('display_errors', 1);
		// }

		// customLogToFile( get_class($this). ' : '.$_SERVER['HTTP_HOST'].'://'.$_SERVER['REQUEST_URI'], '/session_test.log' );
		/* PAGE TYPE */

		if(!isset($this->page_type) ||  !is_array($this->user_permissions) ){
			exit ('ERROR: please set controller permissions and type correctly');
		}

		// Redirect from http to https
		// Also redirect all non-www to www urls except defined controllers
		$ctrl = strtolower(get_class($this));
        $method=$this->router->fetch_method();
		
		//SET LANGUAGE AND LOAD LANG DEFINITIONS		

		
		// load default models and libraries	

		$this->load->library('Email');
		$this->load->library('Auth');
		$this->load->helper('my_helper');

		$data=array();
		
		
		
		if( $this->auth->isLogged() ){
			$data['logged']=true;
			$user_info['logged']=true;			
			$user_info['user_level']=$_SESSION['user_level'];			
		} 
		else {
			$data['logged']=false;
			$user_info['logged']=false;
			$user_info['user_level']=USER_LEVEL_FREE;			
		} 		

		if (strtolower($ctrl) != 'user' && $user_info['user_level'] != USER_LEVEL_SUPERADMIN) {
			redirect('/admin/user');
		}
		
		$this->user_info=$user_info;

		
		/* USER PAGE PERMISSIONS */				
		if(	!permisions($this->user_info['user_level'],$this->user_permissions)){		
			if($this->auth->isLogged()){
				$this->redirectError('You don\'t have permission for this page. ('.$this->user_info['user_level'].')' );				
			}
			else{	
				switch($this->page_type){
					case PAGE_TYPE_FRONT:
					if($this->input->is_ajax_request())
					{
						$ajax_result = array('success'=>false, 'message'=>lang('user_access_denied'));
						$this->av_ajax_return($ajax_result);
					}
					else
					{
						$previous_page = $this->session->userdata('previous_page');	
						$this->session->set_flashdata('message',  lang('user_access_denied'));
						redirect($previous_page);
					}
						
					break;
					case PAGE_TYPE_ADMIN:
						redirect('admin/user/login');
					default:
						redirect('user/login');
					break;
				}
				exit();
			}
		}
		$this->data=$data;		
	}

	public function av_ajax_return( $ajax_result = array() ) {
		
		if($this->input->is_ajax_request()){
			header('Content-Type: application/json');
			error_reporting (0);
			echo json_encode($ajax_result);
		}
		
		else{
			// echo '<html><body>';
			echo 'NON AJAX MODE :<br /><br /><pre>';
			print_r($ajax_result);
			echo '</pre>';
			// echo '</body></html>';
		}

		exit();
	}

	function redirectError($message = "") {
		if(IS_AJAX){
			$res=array();
			$res['success'] = false;
			$res['message'] = $message;
			exit(json_encode($res));
		} else {
			$this->session->set_flashdata('page_error', $message);
			exit($message);	
			redirect('error_page');
		}
	}

	/**
	 * Get Manager using incoming parameter.
	 * If manager is already loaded just return it. Load it otherwise.
	 * 
	 * @param string $manager_name Manager's name same as manager's class name
	 * @return Mixed Object|false
	 **/
	function getManager($manager_name='make_sure_I_exist_in_config'){
		if(isset($this->{$manager_name})){
			return $this->{$manager_name};
		}
		else{
			$this->load->model($manager_name, '', TRUE);	
			//exit('loading manager '.$manager_name);
			return $this->{$manager_name};
		}
		return false;	
	}

}

?>
